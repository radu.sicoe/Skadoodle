using UnityEngine;

[System.Serializable]
public class Item
{
    public string name;
    public string description;
    public Sprite itemImg;
}

public class Collection : MonoBehaviour
{
    public Item item;
    public float healthChange;
    public float moveSpeedChange;
    public float attackSpeedChange;
    public float bulletSizeChange;
    public int coinChange;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<SpriteRenderer>().sprite = item.itemImg;
        Destroy(GetComponent<PolygonCollider2D>());
        gameObject.AddComponent<PolygonCollider2D>();
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.tag == "Player")
        {
            statsController.CoinChange(coinChange);
            statsController.HealPlayer(healthChange);
            statsController.MoveSpeedChange(moveSpeedChange);
            statsController.FireRateChange(attackSpeedChange);
            statsController.BulletSizeChange(bulletSizeChange);
            statsController.instance.UpdateCollectedItems(this);
            Destroy(gameObject);
        }
    }
}

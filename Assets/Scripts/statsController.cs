using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class statsController : MonoBehaviour
{
    public static statsController instance;

    private static float health = 10;
    private static float maxHealth = 10;
    private static float moveSpeed = 5f;
    private static float fireRate = 0.5f;
    private static float bulletSize = 0.5f;
    private static int coinCount = 0;
    private bool bootCollected = false;
    private bool screwCollected = false;
    public List<string> collectedNames = new List<string>();
    public static float Health { get => health; set => health = value; }
    public static float MaxHealth { get => maxHealth; set => maxHealth = value; }
    public static float MoveSpeed { get => moveSpeed; set => moveSpeed = value; }
    public static float FireRate { get => fireRate; set => fireRate = value; }
    public static float BulletSize { get => bulletSize; set => bulletSize = value; }
    public static int CoinCount { get => coinCount; set => coinCount = value; }

    public Text coinsCollectedText;

    // Start is called before the first frame update
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    // Update is called once per frame
    void Update()
    {
        coinsCollectedText.text = CoinCount.ToString();
    }

    public static void DamagePlayer(int damage)
    {
        health -= damage;
        if (health <= 0)
        {
            KillPlayer();
        }
    }

    public static void HealPlayer(float healAmmount)
    {
        health = Mathf.Min(maxHealth, health + healAmmount);
    }

    public void UpdateCollectedItems(Collection item)
    {
        collectedNames.Add(item.item.name);

        foreach(string i in collectedNames)
        {
            switch(i)
            {
                case "Boot":
                    bootCollected = true;
                    break;
                case "Screw":
                    screwCollected = true;
                    break;
            }
        }
        if(screwCollected && bootCollected)
        {
            FireRateChange(0.5f);
        }
    }

    private static void KillPlayer()
    {

    }
    public static void CoinChange(int coin)
    {
        coinCount += coin;
    }
    public static void MoveSpeedChange(float speed)
    {
        moveSpeed += speed;
    }
    public static void FireRateChange(float rate)
    {
        fireRate -= rate;
    }
    public static void BulletSizeChange(float size)
    {
        bulletSize += size;
    }

}

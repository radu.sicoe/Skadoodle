using UnityEngine;
using UnityEngine.UI;

public class playerController : MonoBehaviour
{

    public float speed;
    Rigidbody2D rigidbody;
    public GameObject bulletPreFab;
    public float bulletSpeed;
    private float lastFire;
    public float fireDelay;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        fireDelay = statsController.FireRate;
        speed = statsController.MoveSpeed;
        
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        float shootHoriz = Input.GetAxis("ShootHorizontal");
        float shootVert= Input.GetAxis("ShootVertical");
        if ((shootHoriz != 0 || shootVert != 0) && Time.time > lastFire + fireDelay)
        {
            Shoot(shootHoriz, shootVert);
            lastFire = Time.time;
        }

        rigidbody.velocity = new Vector3(horizontal * speed, vertical * speed, 0);
    }

    void Shoot(float x, float y)
    {
        GameObject bullet = Instantiate(bulletPreFab, transform.position, Quaternion.identity);
        bullet.AddComponent<Rigidbody2D>().gravityScale = 0;
        bullet.GetComponent<Rigidbody2D>().velocity = new Vector3(
            (x < 0) ? Mathf.Floor(x) * bulletSpeed : Mathf.Ceil(x) * bulletSpeed,
            (y < 0) ? Mathf.Floor(y) * bulletSpeed : Mathf.Ceil(y) * bulletSpeed,
            0
            );
    }
}
